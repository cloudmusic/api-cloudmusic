'use strict'

var fs = require('fs');
var path = require('path');
var mongoosePaginate= require('mongoose-pagination');
var Artist = require('../models/artist');
var Album = require('../models/album');
var Song = require('../models/song');

function getArtist(req,res){
	var artistId = req.params.id;

	Artist.findById(artistId, (err,artist)=>{
		if(err){
			return res.status(500).send({message:"Error en el servidor"});
		}
		if(!artist){
			return res.status(404).send({message:"Artista no encontrado"});
		}
		else{
			return res.status(200).send(artist);
		}
	})
}

function saveArtist(req,res){
	var artist = new Artist();

	var params = req.body;
	if(params.name==null || params.description==null){
		return res.status(500).send({message:"Faltan datos de artista."});
	}

	artist.name=params.name;
	artist.description=params.description;
	artist.image="null";

	artist.save((err,artistStored)=>{
		if(err || !artistStored){
			return res.status(500).send({message:"Ocurrio un error en el servidor."});
		}
		else{
			return res.status(200).send({artist:artistStored});
		}

	});
}

function getArtists(req,res){
	var page = req.params.page;
	if(page==null){
		page=1;
	}
	var itemsPerPage=10;

	Artist.find().sort('name').paginate(page, itemsPerPage, function(err,artists,total){
		if(err){
			return res.status(500).send({message:"Error en el servidor"});
		}
		else if(!artists){
			return res.status(404).send({message:"No se encontraron artistas"});
		}
		else{
			return res.status(200).send({
				total_artists:total,
				artists:artists
			});
		}
	})
}

function updateArtist(req,res){
	var artistId=req.params.id;
	var update=req.body;
	//console.log(update);

	Artist.findByIdAndUpdate(artistId,update,(err,artistUpdated)=>{
		if(err){
			return res.status(500).send({message:"Ocurrio un error en el servidor"});
		}
		else if(!artistUpdated){
			return res.status(404).send({message:"Artista no encontrado"});
		}
		else{
			return res.status(200).send({artist:artistUpdated});
		}
	});
}

function deleteArtist(req,res){
	var artistId=req.params.id;

	Artist.findByIdAndRemove(artistId,(err,artistRemoved)=>{
		if(err){
			return res.status(500).send({message:"Ocurrio un error en el servidor."});
		}
		if(artistRemoved){
			Album.find({artist:artistRemoved._id}).remove((err,albumRemoved)=>{
				if(err){
					return res.status(500).send({message:"Ocurrio un error en el servidor."});			
				}
				if(albumRemoved){
					Song.find({album:albumRemoved._id}).remove((err,songRemoved)=>{
						if(err){
							return res.status(500).send({message:"Ocurrio un error en el servidor."});
						}
						if(songRemoved){
							return res.status(200).send({artistRemoved});
						}
					});
				}
			});
		}
		else{
			return res.status(404).send({message:"Artista no encontrado."});
		}
	});
}

function uploadImage(req,res){
	var artistId = req.params.id;
	var file_name = "No subido..";

	if(req.files){
		var file_path = req.files.image.path;
		var file_split = file_path.split('\/');
		file_name = file_split[2];

		var file_ext = file_name.split('\.')[1].toLowerCase();
		if(file_ext=="png" || file_ext=="jpg" || file_ext=="gif"){
			Artist.findByIdAndUpdate(artistId, {image:file_name}, (err,artistUpdated) =>{
				if(err){
					return res.status(500).send({message:"Error en el servidor."});
				}
				if(!artistUpdated){
					return res.status(404).send({message:"Usuario no encontrado"});
				}
				else{
					return res.status(200).send({artist:artistUpdated});
				}
			});
		}
		else{
			return res.status(200).send({message:"No se subio ninguna imagen."});			
		}

		console.log(file_path);
	}else{
		return res.status(200).send({message:"No se subio ninguna imagen."});
	}
}

function getImageFile(req,res){
	var imageFile = req.params.imageFile;
	var path_file = './uploads/artists/'+imageFile;
	fs.exists(path_file, function(exists){
		if(exists){
			return res.sendFile(path.resolve(path_file));
		}
		else{
			return res.status(200).send({message:"No se encontro ninguna imagen."});
		}
	});
}

module.exports = {
	getArtist,
	saveArtist,
	getArtists,
	updateArtist,
	deleteArtist,
	uploadImage,
	getImageFile
}