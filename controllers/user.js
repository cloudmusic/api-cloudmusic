'use strict'

var fs = require('fs');
var path = require('path');
var bcrypt = require('bcrypt-nodejs');
var User = require('../models/user');
var jwt = require('../services/jwt');

function pruebas(req,res){
	res.status(200).send({
		message: 'Hola prueba'
	});
}

function saveUser(req,res){
	var status=true;
	var user = new User();

	var params = req.body;

	if(params.name){
		user.name=params.name;
	}
	else{
		status=false;
	}

	if(params.surname){
		user.surname=params.surname;
	}
	else{
		status=false;
	}

	if(params.email){
		user.email=params.email;
	}
	else{
		status=false;
	}

	if(!status){
		console.log("faltaron datos.");
		console.log(params);
		return res.status(200).send({message:"Faltan datos para registar."});
	}

	user.role="USER_ROLE";
	user.image="null";


	//console.log(params);

	if(params.password){
		bcrypt.hash(params.password,null,null, function(err,hash){
			user.password=hash;
			user.save((err, userStored) => {
				if(err || !userStored){
					return res.status(200).send({message:"Error al guardar."});
				}
				else{
					console.log(userStored);
					return res.status(200).send({user:userStored});
				}
			});
		});
	}
	else{
		return res.status(200).send({message:"Introduce contraseña."});
	}
}

function loginUser(req,res){
	var status=true;
	var params = req.body;

	var email = params.email;
	var password = params.password;

	User.findOne({email: email.toLowerCase()}, (err,user)=>{
		if(err){
			return res.status(500).send({message:"Ocurrio un error."});
		}
		if(user==null || !user || !user.id){
			return res.status(404).send({message:"Usuario no existe"});
		}
		console.log(user);
		bcrypt.compare(password,user.password, function(err,check){
			if(check){
				//console.log(user);
				if(params.getHash){
					return res.status(200).send({token:jwt.createToken(user)});
				}
				else{
					return res.status(200).send({user});
				}
			}
			else{
				return res.status(404).send({message:"Datos de usuario incorrectos"});
			}
		});
	});
}

function updateUser(req,res){
	var userId = req.params.id;
	var update = req.body;

	if (userId!=req.user.sub) {
		return res.status(500).send({message:"No tienes permisos para esta accion."});
	}

	User.findByIdAndUpdate(userId, update, (err,userUpdated) =>{
		if(err){
			return res.status(500).send({message:"Error en el servidor."});
		}
		if(!userUpdated){
			return res.status(404).send({message:"Usuario no encontrado"});
		}
		else{
			return res.status(200).send({user:userUpdated});
		}
	});
}

function uploadImage(req,res){
	var userId = req.params.id;
	var file_name = "No subido..";

	if(req.files){
		var file_path = req.files.image.path;
		var file_split = file_path.split('\/');
		file_name = file_split[2];

		var file_ext = file_name.split('\.')[1].toLowerCase();
		if(file_ext=="png" || file_ext=="jpg" || file_ext=="gif"){
			User.findByIdAndUpdate(userId, {image:file_name}, (err,userUpdated) =>{
				if(err){
					return res.status(500).send({message:"Error en el servidor."});
				}
				if(!userUpdated){
					return res.status(404).send({message:"Usuario no encontrado"});
				}
				else{
					return res.status(200).send({image:file_name,user:userUpdated});
				}
			});
		}
		else{
			return res.status(200).send({message:"No se subio ninguna imagen."});			
		}

		console.log(file_path);
	}else{
		return res.status(200).send({message:"No se subio ninguna imagen."});
	}
}

function getImageFile(req,res){
	var imageFile = req.params.imageFile;
	var path_file = './uploads/users/'+imageFile;
	fs.exists(path_file, function(exists){
		if(exists){
			return res.sendFile(path.resolve(path_file));
		}
		else{
			return res.status(200).send({message:"No se encontro ninguna imagen."});
		}
	});
}

module.exports = {
	pruebas,
	saveUser,
	loginUser,
	updateUser,
	uploadImage,
	getImageFile
};