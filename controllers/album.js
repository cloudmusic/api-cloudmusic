'use strict'

var fs = require('fs');
var path = require('path');
var mongoosePaginate= require('mongoose-pagination');
var Artist = require('../models/artist');
var Album = require('../models/album');
var Song = require('../models/song');

function getAlbum(req,res){
	var albumId= req.params.id;

	Album.findById(albumId).populate({path:"artist"}).exec((err,album)=>{
		if(err){
			return res.status(500).send({message:"Error en el servidor"});
		}
		else if(!album){
			return res.status(404).send({message:"Album no encontrado"});
		}
		else{
			return res.status(200).send({album});
		}
	});
}

function saveAlbum(req,res){
	var album= new Album();
	var params=req.body;

	if(params.title==null || params.description==null || params.year==null || params.artist==null){
		return res.status(200).send({message:"Faltan datos de artista."});
	}

	album.title=params.title;
	album.description=params.description;
	album.year=params.year;
	album.image="null";
	album.artist=params.artist;

	album.save((err,albumStored)=>{
		if(err || !albumStored){
			return res.status(500).send({message:"Ocurrio un error en el servidor."});
		}
		else{
			return res.status(200).send({album:albumStored});
		}
	});
}

function getAlbums(req,res){
	var artistId=req.params.artist;
	if(!artistId){
		var find=Album.find({}).sort('title');
	}
	else{
		var find=Album.find({artist:artistId}).sort('year');
	}
	if(!find){
		return res.status(500).send({message:"Ocurrio un error en el servidor"});
	}
	else{
		find.populate({path: 'artist'}).exec((err,albums)=>{
			if(err){
				return res.status(500).send({message:"Error en la peticion"});
			}
			else if(!albums){
				return res.status(404).send({message:"No se encontraron albums"});	
			}
			else{
				return res.status(200).send({albums});
			}
		});
	}
}

function updateAlbum(req,res){
	var albumId=req.params.id;
	var update=req.body;

	Album.findByIdAndUpdate(albumId,update,(err,albumUpdated)=>{
		if(err){
			return res.status(500).send({message:"Error en el servidor."});
		}
		else{
			if(!albumUpdated){
				return res.status(404).send({message:"No se encontro el album."});		
			}
			else{
				return res.status(200).send({album:albumUpdated});
			}

		}
	})
}

function deleteAlbum(req,res){
	var albumId=req.params.id;
	Album.findByIdAndRemove(albumId,(err,albumRemoved)=>{
		if(err){
			return res.status(500).send({message:"Ocurrio un error en el servidor."});			
		}
		if(albumRemoved){
			Song.find({album:albumRemoved._id}).remove((err,songRemoved)=>{
				if(err){
					return res.status(500).send({message:"Ocurrio un error en el servidor."});
				}
				if(songRemoved){
					return res.status(200).send({albumRemoved});
				}
			});
		}
	});
}

function uploadImage(req,res){
	var albumId = req.params.id;
	var file_name = "No subido..";

	if(req.files){
		var file_path = req.files.image.path;
		var file_split = file_path.split('\/');
		file_name = file_split[2];
		var file_ext = file_name.split('\.')[1].toLowerCase();
		if(file_ext=="png" || file_ext=="jpg" || file_ext=="gif"){
			Album.findByIdAndUpdate(albumId, {image:file_name}, (err,albumUpdated) =>{
				if(err){
					return res.status(500).send({message:"Error en el servidor."});
				}
				if(!albumUpdated){
					return res.status(404).send({message:"Album no encontrado"});
				}
				else{
					return res.status(200).send({album:albumUpdated});
				}
			});
		}
		else{
			return res.status(200).send({message:"No se subio ninguna imagen."});			
		}

		console.log(file_path);
	}else{
		return res.status(200).send({message:"No se subio ninguna imagen."});
	}
}

function getImageFile(req,res){
	var imageFile = req.params.imageFile;
	var path_file = './uploads/albums/'+imageFile;
	fs.exists(path_file, function(exists){
		if(exists){
			return res.sendFile(path.resolve(path_file));
		}
		else{
			return res.status(200).send({message:"No se encontro ninguna imagen."});
		}
	});
}

module.exports={
	getAlbum,
	saveAlbum,
	getAlbums,
	updateAlbum,
	deleteAlbum,
	uploadImage,
	getImageFile
}