'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = process.env.PORT || 3799;

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/proyecto', {useMongoClient: true}, (err,res)=>{
	if(err){
		console.log(err);
		throw err;
	}
	else{
		console.log('Conectado correctamente');

		app.listen(port, function(){			
			console.log("escuchando");
		});
	}
});